// skripti.js

document.addEventListener('DOMContentLoaded', function () {
    const contactForm = document.getElementById('contactForm');
    const submitButton = document.getElementById('submitButton');

    contactForm.addEventListener('submit', function (event) {
        event.preventDefault(); // Estä lomakkeen oletustoiminta

        // Ohjaa käyttäjä toiselle sivulle "vastaus.html" 2 sekunnin kuluttua
        setTimeout(function () {
            window.location.href = 'vastaus.html'; // Tämän sivun tulisi olla olemassa ja sisältää "Vastaus lähetetty" -viesti
        }, 2000);
    });
});